import {Link} from 'react-router-dom'
// import {Row, Col} from 'react-bootstrap'
export default function Error(){
  return (
    <div className = "text-center">
    <h1>Page not found.</h1>
    <p>Please go back to the <Link to = "/">homepage.</Link></p>
    </div>
  )
}
