import { Button, Row, Col } from 'react-bootstrap'
 import { Link } from 'react-router-dom'

export default function Banner(){
	return (
		<Row>
		  <Col className="p-5 text-center">
		      <h1>Planet Shoes </h1>
		      <p>The journey begins with the perfect pair</p>
		       <Button as = {Link} to = "/products" variant="primary">Order Now!</Button>
		   </Col>
		 </Row>
	)
}
