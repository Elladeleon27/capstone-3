import { useState, useEffect, useContext} from 'react';
import { Button, Container, Table, Row, Col } from 'react-bootstrap';
import { Link, useParams, useNavigate }  from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Cart(){

	const { user } = useContext(UserContext);
	const { orderId } = useParams();

	const navigate = useNavigate();

	const [orders, setOrders] = useState();
	const [product, setProduct] = useState();
	const [productName, setProductName] = useState();
	const [unitPrice, setPrice] = useState();
	const [quantity, setQuantity] = useState();

	const checkOut = (orderId, isCheckedOut) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkOut/${orderId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					isCheckedOut: isCheckedOut
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: "Successfully Checked Out",
					icon: "success"
				}).then(() => {
					window.location.reload();
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Pleas try again."
				})
			}
		})
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/orders/`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrders(data);
		})
	}, [setOrders])

	return (

			<Table hover bordered>
				<thead>
					<tr>
			            <th>Order No.</th>
			            <th>Products</th>
			            <th></th>
			        </tr>
				</thead>
				<tbody>
					{
						orders && orders.map(order => (
			                    <tr key={order._id}>
			                        <td>{order._id}</td>
			                        <td>
			                        	<Table bordered>
			                        		<thead>
			                        			<tr>
                        			                <th>Product ID</th>
                        			                <th>Quantity</th>
                        			                <th>Subtotal</th>
                        			              </tr>
			                        		</thead>
			                        	
			                        	<tbody>
			                        		{
			                        		    order.products.map(product => (
		                        		           	<tr key={product._id} onClick={() => setProduct(product)}>
		                        		           	    <td className="text-center">{product.productId}</td>
		                        		           	    <td className="text-center">{product.quantity}</td>
		                        		           	    <td className="text-center">{product.subtotal}</td>
		                        		           	</tr>
			                        		    ))


			                        		}
			                        		<tr>
                        		                <td className="text-center">Total Amount</td>
                        		                <td className="text-center" colSpan={3}>{order.products.reduce((acc, curr) => acc + curr.subtotal, 0)}</td>
                        		            </tr>
			                        	</tbody>
			                        	</Table>
			                        </td>
			                        <td>
			                        	{(order.isCheckedOut === false) ?
			                        		<Button variant="outline-success" onClick={() => checkOut(order._id, true)}>Check Out</Button> 
			                        		:
			                        		'To ship'
			                        	}
			                        </td>
			                        
			                    </tr>
			            ))
					}
				</tbody>
			</Table>
					


			
	
	)
}
