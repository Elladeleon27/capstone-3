import { Row, Col, Card, Carousel} from 'react-bootstrap';

export default function Highlights() {
   return (
   <Row>
       <Col>
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://i.pinimg.com/736x/92/29/8a/92298a3401410b8249ab58c9105f1459.jpg"
          alt="Nike Air Jordan"
        />
        <Carousel.Caption>
          <h3>Air Jordan 1</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://cdn.shopify.com/s/files/1/1763/5727/products/IMG_1843_900x.jpg?v=1571855732"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Air Force 1</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://www.sneakersnstuff.com/images/148856/fotostudio-juli-29-2016-1574.jpg"
          alt="Nike Pegasus"
        />

        <Carousel.Caption>
          <h3>Nike Pegasus</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
       </Col>
   </Row>


       )
 }

